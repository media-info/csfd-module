import {MediaInfoModule, MediaItemId} from '@media-info/types';
import CSFD, {CSFDItemProps, Locale} from '@media-info/csfd-api';

export class CSFDModule implements MediaInfoModule<CSFDItemProps> {
    api: CSFD;

    constructor(locale: Locale) {
        this.api = new CSFD(locale);
    }

    async get(id: MediaItemId) {
        const csfdItem = this.api.get(id);
        const csfdData = await csfdItem.fetch(true);
        return {
            id,
            ...csfdData
        }
    }
}
